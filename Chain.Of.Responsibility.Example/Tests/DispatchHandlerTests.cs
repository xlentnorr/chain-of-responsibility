﻿using Chain.Of.Responsibility.Example.Chain.Pattern.Extended;
using Chain.Of.Responsibility.Example.Models;
using FluentAssertions;
using System;
using Xunit;

namespace Chain.Of.Responsibility.Example.Tests
{
    public class DispatchHandlerTests
    {
        /// <summary>
        /// BeforeEndDateValidationHandler
        /// </summary>
        [Fact]
        public void Registration_fails_for_short_term_user_if_end_date_has_passed()
        {
            var shortTermUser = new ShortTermUser
            {
                EndDate = DateTime.Now.AddDays(-1)
            };

            Action act = () => new BeforeEndDateValidationHandler().Handle(shortTermUser);

            act.Should().Throw<Exception>()
                .WithMessage($"{nameof(shortTermUser.EndDate)} has passed.");
        }

        /// <summary>
        /// BeforeEndDateValidationHandler
        /// </summary>
        [Fact]
        public void Registration_succeeds_for_short_term_user_if_end_date_has_not_passed()
        {
            var shortTermUser = new ShortTermUser
            {
                EndDate = DateTime.Now
            };

            Action act = () => new BeforeEndDateValidationHandler().Handle(shortTermUser);

            act.Should().NotThrow<Exception>();
        }
        /// <summary>
        /// SeniorDateOfEmploymentValidationHandler
        /// </summary>
        [Fact]
        public void Registration_fails_for_senior_user_if_employment_date_is_too_early()
        {
            var seniorUser = new SeniorUser
            {
                DateOfEmployment = DateTime.Now
            };

            Action act = () => new SeniorDateOfEmploymentValidationHandler().Handle(seniorUser);

            act.Should().Throw<Exception>()
                .WithMessage($"{nameof(seniorUser.DateOfEmployment)} is too early to be eligible.");
        }

        /// <summary>
        /// SeniorDateOfEmploymentValidationHandler
        /// </summary>
        [Fact]
        public void Registration_succeeds_for_senior_user_if_employment_date_is_eligible()
        {
            var seniorUser = new SeniorUser
            {
                DateOfEmployment = DateTime.Now.AddYears(-5)
            };

            Action act = () => new SeniorDateOfEmploymentValidationHandler().Handle(seniorUser);

            act.Should().NotThrow<Exception>();
        }

        /// <summary>
        /// Dispatch handler
        /// </summary>
        [Fact]
        public void Registration_succeeds_for_valid_user()
        {
            var user = new User
            {
                Username = "Jane Doe",
                Password = "password",
                BirthDate = DateTime.Now.AddYears(-18)
            };

            Action act = () => new DispatchHandler().Handle(user);

            act.Should().NotThrow();
        }

        /// <summary>
        /// Dispatch handler
        /// </summary>
        [Fact]
        public void Registration_succeeds_for_valid_short_term_user()
        {
            var user = new ShortTermUser
            {
                Username = "Jane Doe",
                Password = "password",
                BirthDate = DateTime.Now.AddYears(-18),
                EndDate = DateTime.Now.AddDays(1)
            };

            Action act = () => new DispatchHandler().Handle(user);

            act.Should().NotThrow();
        }

        /// <summary>
        /// Dispatch handler
        /// </summary>
        [Fact]
        public void Registration_succeeds_for_valid_senior_user()
        {
            var user = new SeniorUser
            {
                Username = "Jane Doe",
                Password = "password",
                BirthDate = DateTime.Now.AddYears(-18),
                DateOfEmployment = DateTime.Now.AddYears(-5)
            };

            Action act = () => new DispatchHandler().Handle(user);

            act.Should().NotThrow();
        }

        /// <summary>
        /// ExtendedRegistrationProcessor
        /// with Extended handler
        /// </summary>
        [Fact]
        public void Registration_fails_for_new_user_with_start_date_later_than_today()
        {
            var newUser = new NewUser
            {
                Username = "Jane Doe",
                Password = "password",
                BirthDate = DateTime.Now.AddYears(-18),
                StartDate = DateTime.Now.AddDays(1)
            };

            Action act = () => new ExtendedRegistrationProcessor().Register(newUser);

            act.Should().Throw<Exception>()
                .WithMessage($"Too early. Try again at {newUser.StartDate.Date}.");
        }

        /// <summary>
        /// ExtendedRegistrationProcessor
        /// with Extended handler
        /// </summary>
        [Fact]
        public void Registration_succeeds_for_new_user_with_start_date_today_or_earlier()
        {
            var newUser = new NewUser
            {
                Username = "Jane Doe",
                Password = "password",
                BirthDate = DateTime.Now.AddYears(-18),
                StartDate = DateTime.Today
            };

            Action act = () => new ExtendedRegistrationProcessor().Register(newUser);

            act.Should().NotThrow<Exception>();
        }
    }
}
