﻿using Chain.Of.Responsibility.Example.Models;
using FluentAssertions;
using System;
using Xunit;

namespace Chain.Of.Responsibility.Example.Tests
{
    public class BasicUserRegistrationTests
    {
        [Fact]
        public void When_Username_Is_Empty_Then_Exception_Should_Be_Thrown()
        {
            var user = new User();

            Action act = () => new BasicUserRegistrationProcessor().Register(user);

            act.Should().Throw<Exception>();
        }

        [Fact]
        public void When_Password_Is_Empty_Then_Exception_Should_Be_Thrown()
        {
            var user = new User { Username = "Jane Doe" };

            Action act = () => new BasicUserRegistrationProcessor().Register(user);

            act.Should().Throw<Exception>();
        }

        [Fact]
        public void When_BirthDate_Is_Not_Earlier_Than_18_Years_Ago_Then_Exception_Should_Be_Thrown()
        {
            var user = new User
            {
                Username = "Jane Doe",
                Password = Guid.NewGuid().ToString(),
                BirthDate = DateTime.Now
            };

            Action act = () => new BasicUserRegistrationProcessor().Register(user);

            act.Should().Throw<Exception>();
        }

        [Fact]
        public void When_All_Properties_Are_valid_Then_Exception_Should_NOT_Be_Thrown()
        {
            var user = new User
            {
                Username = "Jane Doe",
                Password = Guid.NewGuid().ToString(),
                BirthDate = DateTime.Now.AddYears(-20)
            };

            Action act = () => new BasicUserRegistrationProcessor().Register(user);

            act.Should().NotThrow<Exception>();
        }
    }
}