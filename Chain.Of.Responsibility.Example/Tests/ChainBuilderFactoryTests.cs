﻿using Chain.Of.Responsibility.Example.Chain.Pattern.Extended;
using Chain.Of.Responsibility.Example.Chain.Pattern.Registration;
using Chain.Of.Responsibility.Example.Models;
using FluentAssertions;
using System;
using System.Collections.Generic;
using Xunit;

namespace Chain.Of.Responsibility.Example.Tests
{
    /// <summary>
    /// Specification test
    /// http://www.sustainabletdd.com/2012/08/testing-chain-of-responsibility-part-2.html
    /// http://www.sustainabletdd.com/2015/11/structure-of-tests-as-specifications.html
    /// </summary>
    public class ChainBuilderFactoryTests
    {
        [Fact]
        public void Test_factory_returns_proper_chain_of_handlers()
        {
            var factory = new TestableChainBuilderFactory();
            const int expectedChainLength = 3;
            var expectedCollection = new List<Type>
            {
                typeof(UsernameRequiredValidationHandler),
                typeof(PasswordRequiredValidationHandler),
                typeof(OnlyAdultValidationHandler)
            };

            var chain = factory.GetHandler();

            var log = LoggingMockHandler.Log;
            log.Reset();

            chain.Handle(new User());

            log.AssertSize(expectedChainLength);
            for (var i = 0; i < expectedCollection.Count; i++)
            {
                log.AssertAtPosition(expectedCollection[i], i);
            }
        }


        private class TestableChainBuilderFactory : ChainBuilderFactory
        {
            protected override IHandler<BaseUser> MakeFirstHandler(IHandler<BaseUser> nextHandler)
            {
                return new LoggingMockHandler(typeof(UsernameRequiredValidationHandler), nextHandler);
            }

            protected override IHandler<BaseUser> MakeSecondHandler(IHandler<BaseUser> nextHandler)
            {
                return new LoggingMockHandler(typeof(PasswordRequiredValidationHandler), nextHandler);
            }

            protected override IHandler<BaseUser> MakeLastHandler()
            {
                var mock = new LoggingMockHandler(typeof(OnlyAdultValidationHandler), null);

                return mock;
            }
        }

        private class LoggingMockHandler : Handler<BaseUser>
        {
            private readonly Type _myType;
            public static readonly Log Log = new();

            public LoggingMockHandler(Type type, IHandler<BaseUser> nextHandler)
            {
                _myType = type;
                SetNext(nextHandler);
            }

            public override void Handle(BaseUser request)
            {
                Log.Add(_myType);
                base.Handle(request);
            }
        }

        private class Log
        {
            private List<Type> _typeList;

            public void Reset()
            {
                _typeList = new List<Type>();
            }

            public void Add(Type type)
            {
                _typeList.Add(type);
            }

            public void AssertSize(int expectedSize)
            {
                _typeList.Count.Should().Be(expectedSize);
            }

            public void AssertAtPosition(Type expectedType, int position)
            {
                expectedType.Should().Be(_typeList[position]);
            }
        }
    }
}
