﻿using Chain.Of.Responsibility.Example.Chain.Pattern.Registration;
using Chain.Of.Responsibility.Example.Models;
using FluentAssertions;
using System;
using Xunit;

namespace Chain.Of.Responsibility.Example.Tests
{
    public class ChainPatternRegistrationTests
    {
        [Fact]
        public void When_Username_is_empty_then_Exception_should_be_thrown()
        {
            var user = new User { Username = string.Empty };

            Action act = () => new UsernameRequiredValidationHandler().Handle(user);

            act.Should().Throw<Exception>()
                .WithMessage("Username is required.");
        }

        [Fact]
        public void When_Username_is_provided_then_Exception_should_NOT_be_thrown()
        {
            var user = new User { Username = "Jane Doe" };

            Action act = () => new UsernameRequiredValidationHandler().Handle(user);

            act.Should().NotThrow<Exception>();
        }

        [Fact]
        public void When_Password_is_empty_then_Exception_should_be_thrown()
        {
            var user = new User { Password = string.Empty };

            Action act = () => new PasswordRequiredValidationHandler().Handle(user);

            act.Should().Throw<Exception>()
                .WithMessage("Password is required.");
        }

        [Fact]
        public void When_Password_is_provided_then_Exception_should_NOT_be_thrown()
        {
            var user = new User { Password = Guid.NewGuid().ToString() };

            Action act = () => new PasswordRequiredValidationHandler().Handle(user);

            act.Should().NotThrow<Exception>();
        }

        [Fact]
        public void When_BirthDate_is_NOT_earlier_than_18_years_ago_then_Exception_should_be_thrown()
        {
            var user = new User { BirthDate = DateTime.Now };

            Action act = () => new OnlyAdultValidationHandler().Handle(user);

            act.Should().Throw<Exception>()
                .WithMessage("Age under 18 is not allowed.");
        }

        [Fact]
        public void When_BirthDate_is_earlier_than_18_years_ago_then_Exception_should_NOT_be_thrown()
        {
            var user = new User { BirthDate = DateTime.Now.AddYears(-20) };

            Action act = () => new OnlyAdultValidationHandler().Handle(user);

            act.Should().NotThrow<Exception>();
        }

        [Fact]
        public void When_all_Properties_are_valid_then_Exception_should_NOT_be_thrown()
        {
            var user = new User
            {
                Username = "Jane Doe",
                Password = Guid.NewGuid().ToString(),
                BirthDate = DateTime.Now.AddYears(-20)
            };

            Action act = () => new UserRegistrationProcessor().Register(user);

            act.Should().NotThrow<Exception>();
        }

        [Fact]
        public void When_any_Property_is_invalid_then_Exception_should_be_thrown()
        {
            var emptyPassword = string.Empty;
            var user = new User
            {
                Username = "Jane Doe",
                Password = emptyPassword,
                BirthDate = DateTime.Now.AddYears(-20)
            };

            Action act = () => new UserRegistrationProcessor().Register(user);

            act.Should().Throw<Exception>()
                .WithMessage("Password is required.");
        }
    }
}