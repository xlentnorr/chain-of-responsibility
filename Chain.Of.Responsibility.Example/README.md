# Chain.Of.Responsibility.Example

Project contains four version of example of validation users registration. 
BasicUserRegistrationProcessor.cs - Coupled Validations.
ChainPatternRegistrationProcessor.cs - Decoupled Validations. 
DispatchHandler.cs - Decoupled Validations with subclassed requests. 
ExtendedHandler.cs - Decoupled Validations with subclassed handlers.
