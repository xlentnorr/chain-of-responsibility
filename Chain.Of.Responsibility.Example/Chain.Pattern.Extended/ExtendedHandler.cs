﻿using Chain.Of.Responsibility.Example.Models;
using System;

namespace Chain.Of.Responsibility.Example.Chain.Pattern.Extended
{
    public class ExtendedHandler : DispatchHandler
    {
        public override void Handle(BaseUser request)
        {
            if (request is NewUser newUser)
                if (newUser.StartDate > DateTime.Today)
                    throw new Exception($"Too early. Try again at {newUser.StartDate.Date}.");

            base.Handle(request);
        }
    }
}
