﻿using Chain.Of.Responsibility.Example.Chain.Pattern.Registration;
using Chain.Of.Responsibility.Example.Models;

namespace Chain.Of.Responsibility.Example.Chain.Pattern.Extended
{
    public class ChainBuilderFactory
    {
        public IHandler<BaseUser> GetHandler()
        {
            return MakeFirstHandler(MakeSecondHandler(MakeLastHandler()));
        }

        protected virtual IHandler<BaseUser> MakeFirstHandler(IHandler<BaseUser> nextHandler)
        {
            var handler = new UsernameRequiredValidationHandler();
            handler.SetNext(nextHandler);
            return handler;
        }

        protected virtual IHandler<BaseUser> MakeSecondHandler(IHandler<BaseUser> nextHandler)
        {
            var handler = new PasswordRequiredValidationHandler();
            handler.SetNext(nextHandler);
            return handler;
        }

        protected virtual IHandler<BaseUser> MakeLastHandler()
        {
            return new OnlyAdultValidationHandler();
        }
    }
}