﻿using Chain.Of.Responsibility.Example.Models;

namespace Chain.Of.Responsibility.Example.Chain.Pattern.Extended
{
    public class BaseUserRegistrationProcessor
    {
        public void Register(BaseUser user)
        {
            var handlerFactory = new ChainBuilderFactory();
            var handler = handlerFactory.GetHandler();

            handler.Handle(user);
        }
    }
}