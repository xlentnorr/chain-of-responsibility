﻿using Chain.Of.Responsibility.Example.Models;

namespace Chain.Of.Responsibility.Example.Chain.Pattern.Extended
{
    public class ExtendedRegistrationProcessor
    {
        public void Register(BaseUser user)
        {
            var handler = new ExtendedHandler();
            handler.SetNext(new DispatchHandler());

            handler.Handle(user);
        }
    }
}
