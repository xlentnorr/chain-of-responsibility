﻿using System;
using Chain.Of.Responsibility.Example.Chain.Pattern.Registration;
using Chain.Of.Responsibility.Example.Models;

namespace Chain.Of.Responsibility.Example.Chain.Pattern.Extended
{
    public class SeniorDateOfEmploymentValidationHandler : Handler<SeniorUser>
    {
        public override void Handle(SeniorUser seniorUser)
        {
            if (seniorUser.DateOfEmployment >= DateTime.Now.AddYears(-5))
                throw new Exception($"{nameof(seniorUser.DateOfEmployment)} is too early to be eligible.");

            base.Handle(seniorUser);
        }
    }
}