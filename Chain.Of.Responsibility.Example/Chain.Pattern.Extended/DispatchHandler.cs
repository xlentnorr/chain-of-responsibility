﻿using Chain.Of.Responsibility.Example.Chain.Pattern.Registration;
using Chain.Of.Responsibility.Example.Models;

namespace Chain.Of.Responsibility.Example.Chain.Pattern.Extended
{
    public class DispatchHandler : Handler<BaseUser>
    {
        public override void Handle(BaseUser request)
        {
            switch (request)
            {
                case ShortTermUser shortTermUser:
                    new BeforeEndDateValidationHandler().Handle(shortTermUser);
                    break;
                case SeniorUser seniorUser:
                    new SeniorDateOfEmploymentValidationHandler().Handle(seniorUser);
                    break;
            }

            new BaseUserRegistrationProcessor().Register(request);
        }
    }
}
