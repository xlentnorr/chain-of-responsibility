﻿using Chain.Of.Responsibility.Example.Chain.Pattern.Registration;
using Chain.Of.Responsibility.Example.Models;
using System;

namespace Chain.Of.Responsibility.Example.Chain.Pattern.Extended
{
    public class BeforeEndDateValidationHandler : Handler<ShortTermUser>
    {
        public override void Handle(ShortTermUser shortTermUser)
        {
            if (shortTermUser.EndDate < DateTime.Today)
                throw new Exception($"{nameof(shortTermUser.EndDate)} has passed.");

            base.Handle(shortTermUser);
        }
    }
}