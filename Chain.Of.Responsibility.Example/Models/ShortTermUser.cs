﻿using System;

namespace Chain.Of.Responsibility.Example.Models
{
    public class ShortTermUser : BaseUser
    {
        public DateTime EndDate { get; set; }
    }
}
