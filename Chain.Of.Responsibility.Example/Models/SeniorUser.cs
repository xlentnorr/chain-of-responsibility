﻿using System;

namespace Chain.Of.Responsibility.Example.Models
{
    public class SeniorUser : BaseUser
    {
        public DateTime DateOfEmployment { get; set; }
    }
}
