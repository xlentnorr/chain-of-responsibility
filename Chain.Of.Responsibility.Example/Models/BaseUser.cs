﻿using System;

namespace Chain.Of.Responsibility.Example.Models
{
    public abstract class BaseUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime BirthDate { get; set; }
    }
}