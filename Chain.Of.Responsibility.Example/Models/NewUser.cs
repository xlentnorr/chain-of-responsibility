﻿using System;

namespace Chain.Of.Responsibility.Example.Models
{
    public class NewUser : BaseUser
    {
        public DateTime StartDate { get; set; }
    }
}
