﻿using Chain.Of.Responsibility.Example.Models;

namespace Chain.Of.Responsibility.Example.Chain.Pattern.Registration
{
    public class UserRegistrationProcessor
    {
        public void Register(User user)
        {
            var handler = new UsernameRequiredValidationHandler();
            handler.SetNext(new PasswordRequiredValidationHandler())
                   .SetNext(new OnlyAdultValidationHandler());

            handler.Handle(user);
        }
    }
}