﻿using Chain.Of.Responsibility.Example.Models;
using System;

namespace Chain.Of.Responsibility.Example.Chain.Pattern.Registration
{
    public class UsernameRequiredValidationHandler : Handler<BaseUser>
    {
        public override void Handle(BaseUser user)
        {
            if (string.IsNullOrWhiteSpace(user.Username))
            {
                throw new Exception("Username is required.");
            }

            base.Handle(user);
        }
    }
}