﻿namespace Chain.Of.Responsibility.Example.Chain.Pattern.Registration
{
    public interface IHandler<T>
    {
        void Handle(T request);
        IHandler<T> SetNext(IHandler<T> next);
    }
}