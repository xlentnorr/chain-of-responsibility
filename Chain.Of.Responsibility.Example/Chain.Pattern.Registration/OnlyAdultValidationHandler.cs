﻿using Chain.Of.Responsibility.Example.Models;
using System;

namespace Chain.Of.Responsibility.Example.Chain.Pattern.Registration
{
    public class OnlyAdultValidationHandler : Handler<BaseUser>
    {
        public override void Handle(BaseUser user)
        {
            if (user.BirthDate.Year > DateTime.Now.Year - 18)
            {
                throw new Exception("Age under 18 is not allowed.");
            }

            base.Handle(user);
        }
    }
}