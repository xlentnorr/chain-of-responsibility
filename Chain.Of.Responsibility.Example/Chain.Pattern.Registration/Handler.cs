﻿namespace Chain.Of.Responsibility.Example.Chain.Pattern.Registration
{
    public abstract class Handler<T> : IHandler<T>
    {
        private IHandler<T> Next { get; set; }

        public virtual void Handle(T request)
        {
            Next?.Handle(request);
        }

        public IHandler<T> SetNext(IHandler<T> next)
        {
            return Next = next;
        }
    }
}