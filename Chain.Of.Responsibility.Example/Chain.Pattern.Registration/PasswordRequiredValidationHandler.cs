﻿using Chain.Of.Responsibility.Example.Models;
using System;

namespace Chain.Of.Responsibility.Example.Chain.Pattern.Registration
{
    public class PasswordRequiredValidationHandler : Handler<BaseUser>
    {
        public override void Handle(BaseUser user)
        {
            if (string.IsNullOrWhiteSpace(user.Password))
            {
                throw new Exception("Password is required.");
            }

            base.Handle(user);
        }
    }
}