﻿using System;

namespace ChainOfResponsibilityPattern
{
    class Program
    {
        /// <summary>
        /// Client
        /// </summary>
        static void Main()
        {
            //TODO: Setting up the chain should preferably not be a responsibility for the client.
            ClaimApprover junior = new Junior();
            ClaimApprover manager = new Manager();
            ClaimApprover boss = new Boss();
            junior.SetSuccessor(manager);
            manager.SetSuccessor(boss);

            var c1 = new Claim { Amount = 500, Id = 1 };
            var c2 = new Claim { Amount = 10_000, Id = 2 };
            var c3 = new Claim { Amount = 20_000, Id = 3 };

            Console.WriteLine($"{nameof(Claim)} {c1.Id} asked for approval.");
            junior.ApproveRequest(c1);
            Console.WriteLine();

            Console.WriteLine($"{nameof(Claim)} {c2.Id} asked for approval.");
            manager.ApproveRequest(c2);
            Console.WriteLine();

            Console.WriteLine($"{nameof(Claim)} {c3.Id} asked for approval.");
            junior.ApproveRequest(c3);
            Console.ReadLine();
        }
    }

    /// <summary>
    /// Concrete Handler
    /// </summary>
    public class Junior : ClaimApprover
    {
        public override void ApproveRequest(Claim claim)
        {
            Console.WriteLine($"{GetType().Name} cannot approve any {nameof(Claim)}");

            Successor?.ApproveRequest(claim);

            Console.WriteLine($"{GetType().Name} prints receipt for {nameof(Claim)} {claim.Amount}.");
        }
    }

    /// <summary>
    /// Concrete Handler
    /// </summary>
    public class Manager : ClaimApprover
    {
        public override void ApproveRequest(Claim claim)
        {
            if (claim.Amount is >= 100 and <= 1_000)
                Console.WriteLine(
                    $"{nameof(Claim)} {claim.Id} with amount {claim.Amount} is approved by {GetType().Name}");
            else
            {
                Console.WriteLine($"{GetType().Name} cannot approve {nameof(Claim.Amount)} {claim.Amount}");
                Successor?.ApproveRequest(claim);
            }

        }
    }

    /// <summary>
    /// Concrete Handler
    /// </summary>
    public class Boss : ClaimApprover
    {
        public override void ApproveRequest(Claim claim)
        {
            Console.WriteLine(claim.Amount is > 1_000 and <= 10_000
                ? $"{nameof(Claim)} {claim.Id} with amount {claim.Amount} is approved by {GetType().Name}"
                : $"Exceptional approval for {nameof(Claim)} {claim.Id} with amount {claim.Amount} is approved by {GetType().Name}");
        }
    }

    /// <summary>
    /// Handler
    /// </summary>
    public abstract class ClaimApprover
    {
        protected ClaimApprover Successor;

        public void SetSuccessor(ClaimApprover claimApprover)
        {
            Successor = claimApprover;
        }

        public abstract void ApproveRequest(Claim claim);
    }

    public class Claim
    {
        public int Id { get; set; }
        public double Amount { get; set; }
    }
}
